boot_command = [
  "<up><wait><tab><wait> ",
  "inst.text ",
  "inst.ks=http://{{ .HTTPIP }}:{{ .HTTPPort }}/ks.cfg ",
  "inst.cmdline ",
  "<enter><wait>"
]
cpus = "2"
disk_size = "40000"
format = "ova"
guest_additions_mode = "disable"
guest_os_type = "RedHat_64"
headless = false
http_directory = "http"
iso_checksum = "e524329700abe47ce1f509bed7e2d3c68b336a54c712daa1b492b2429a64d419"
iso_url = "https://repo.almalinux.org/almalinux/8.10/isos/x86_64/AlmaLinux-8.10-x86_64-minimal.iso"
memory = "2048"
ssh_pass = "vagrant"
shutdown_command = "echo 'vagrant' | sudo -S /sbin/halt -h -p"
ssh_timeout = "30m"
ssh_user = "vagrant"
vm_name = "almalinux8"
vboxmanage_nat = "--nat-localhostreachable1"
vboxmanage_on = "on"
provision_file_destination = "/tmp/"
provision_file_source = "id_ecdsa.pub"
post_processor_keep_input_artifact = false
post_processor_provider_override = "virtualbox"
post_processor_output = "almalinux8-lvm.box"
ssh_private_key_file  = "~/.ssh/vagrant_key_ecdsa"