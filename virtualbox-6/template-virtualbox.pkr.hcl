packer {
    required_plugins {
      virtualbox = {
        source  = "github.com/hashicorp/virtualbox"
        version = "~> 1"
      }
      vagrant = {
        source  = "github.com/hashicorp/vagrant"
        version = "~> 1"
      }
    }
  }

variable "boot_command" {
    type    = list(string)
}

variable "cpus" {
    type    = string
    default = "1"
}

variable "disk_size" {
    type    = string
    default = "20000"
}

variable "format" {
    type    = string
    default = "ova"
}

variable "guest_additions_mode" {
    type    = string
}

variable "guest_os_type" {
    type    = string
}

variable "headless" {
    type    = bool
    default = true
}

variable "http_directory" {
    type    = string
}

variable "iso_checksum" {
    type    = string
}

variable "iso_url" {
    type    = string
}

variable "memory" {
    type    = string
    default = "1024"
}

variable "shutdown_command" {
    type    = string
}

variable "ssh_pass" {
    type    = string
    default = "vagrant"
    sensitive = true
}

variable "ssh_timeout" {
    type    = string
    default = "20m"
}

variable "ssh_user" {
    type    = string
    default = "vagrant"
}

variable "vm_name" {
    type    = string
}


variable "provision_file_destination" {
    type    = string
}

variable "provision_file_source" {
    type    = string
}

variable "post_processor_keep_input_artifact" {
    type    = bool
    default = false
}

variable "post_processor_provider_override" {
    type    = string
    default = "virtualbox"
}

variable "post_processor_output" {
    type    = string
}

variable "provision_file_destination_cloud_init" {
    type = string
    default = "/tmp/cloud-config.yml"
}

variable "provision_file_source_cloud_init" {
    type = string
    default = "cloud-config.yml"
}

variable "output_directory" {
    type    = string
    default = "output-linux"
}

variable "ssh_private_key_file" {
    type    = string
}

variable "sudo_cmd" {
    type    = string
    default = "sudo"
}

source "virtualbox-iso" "linux" {
    boot_command            = var.boot_command
    cpus                    = var.cpus
    disk_size               = var.disk_size
    format                  = var.format
    guest_additions_mode    = var.guest_additions_mode
    guest_os_type           = var.guest_os_type
    headless                = var.headless
    http_directory          = var.http_directory
    iso_checksum            = var.iso_checksum
    iso_url                 = var.iso_url
    memory                  = var.memory
    ssh_password            = var.ssh_pass
    shutdown_command        = var.shutdown_command
    ssh_timeout             = var.ssh_timeout
    ssh_username            = var.ssh_user
    ssh_private_key_file    = var.ssh_private_key_file
    vm_name                 = var.vm_name
    output_directory        = var.output_directory
}
  
build {
    sources = ["source.virtualbox-iso.linux"]

    provisioner "file" {
        destination = var.provision_file_destination
        source = var.provision_file_source
    }

    provisioner "file" {
        source      = var.provision_file_source_cloud_init
        destination = var.provision_file_destination_cloud_init
    }

    provisioner "shell" {
        inline = [
            "${var.sudo_cmd} mv ${var.provision_file_destination_cloud_init} /etc/cloud/cloud.cfg.d/99_custom.cfg",
            "${var.sudo_cmd}  cloud-init clean",
            "${var.sudo_cmd}  cloud-init init --local",
            "${var.sudo_cmd}  cloud-init init",
            "${var.sudo_cmd}  cloud-init modules --mode config",
            "${var.sudo_cmd}  cloud-init modules --mode final"
        ]
    }

    post-processor "vagrant" {
        keep_input_artifact = false
        provider_override = var.post_processor_provider_override
        output = var.post_processor_output
    }
}
  
  